<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @access public
 * @author LAcuna
 *
 */
class Template {

    /**
     * @access public 
    */
    protected $CI;
    
    public function __construct() {
         $this->CI = & get_instance();
    }

    public function portal($view, $data = NULL) {
        $this->CI->load->view('template/portal_header');
        $this->CI->load->view($view, $data);
        $this->CI->load->view('template/portal_footer');
    }

    public function auth($view, $data = NULL) {
        $this->CI->load->view('template/auth_header');
        $this->CI->load->view($view, $data);
        $this->CI->load->view('template/auth_footer');
    }
}