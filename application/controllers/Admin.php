<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'form', 'file', 'string'));
        $this->load->library(array('form_validation', 'session','template', 'encrypt'));
        $this->load->model(array('admin_model'));

        $this->valid_session();
    }

    //======================================================================
    // WORK ORDERS
    //======================================================================


    //-----------------------------------------------------
    // Sub-Category work_orders WORK ORDERS
	//-----------------------------------------------------

    public function work_orders(){
        $data['info_work_orders'] = $this->admin_model->get_work_orders_entries();
        $this->template->portal('admin/work_orders', $data);
    }

    //-----------------------------------------------------
    // Sub-Category work_orders WORK ORDERS
    //-----------------------------------------------------
    
    public function update_work_order($id = NULL, $email = NULL)
	{
        $this->form_validation->set_rules('status', 'Estado', 'required');

        $this->form_validation->set_error_delimiters('<small class="form-text " style="color:red">', '</small>');

        if ($this->form_validation->run() == FALSE) {
            $this->template->portal('admin/update_work_order');
        } else {
            $status     = $this->input->post('status');
            $order      = $this->encrypt->decode($id);

            $resp = $this->admin_model->update_work_order_entries($order, $status);

            if(!$resp){
                $this->session->set_flashdata('message_error', 'Intente nuevamente.');
            }else{

                if($status == 3){
                  mail($this->encrypt->decode($email), 'IMEI FAST', 'Su orden de trabajo a sido procesada con éxito.');
                }
                $this->session->set_flashdata('message_success', 'Procedimiento realizado con exito.');
            }
            redirect('admin/work_orders');
        }
    }

    //-----------------------------------------------------
    // Sub-Category remove_work_order WORK ORDERS
    //-----------------------------------------------------
    
    public function remove_work_order($id = NULL)
	{
        $order = $this->encrypt->decode($id);
        if(!is_numeric($order) ){
            $this->session->set_flashdata('message_error', 'Intente nuevamente.');
        }else{
            $resp = $this->admin_model->delete_work_order_entries($order);

            if(!$resp){
                $this->session->set_flashdata('message_error', 'Intente nuevamente.');
            }else{
                $this->session->set_flashdata('message_success', 'Procedimiento realizado con exito.');
            }
        }
        redirect('admin/work_orders');
    }
	
	//======================================================================
    // USERS
    //======================================================================


    //-----------------------------------------------------
    // Sub-Category user_registration USERS
	//-----------------------------------------------------
	
	public function users()
	{
        $data['info_users'] = $this->admin_model->get_users_entries();
        $this->template->portal('admin/users', $data);
    }

    //-----------------------------------------------------
    // Sub-Category user_registration USERS
	//-----------------------------------------------------
	
	public function add_user()
	{
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[m_users.email]');
        $this->form_validation->set_rules('phone', 'Telefono', 'required');
        $this->form_validation->set_rules('name', 'Nombre', 'required');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');

        $this->form_validation->set_error_delimiters('<small class="form-text " style="color:red">', '</small>');

        if ($this->form_validation->run() == FALSE) {
            $this->template->portal('admin/add_user');
        } else {
            $email       = $this->input->post('email'); 
            $phone       = $this->input->post('phone');
            $name        = $this->input->post('name');
            $password    = $this->input->post('password');

            $resp_user = $this->admin_model->insert_user_entry($email, $phone, $name, $password);

            if(!$resp_user){
                $this->session->set_flashdata('message_error', 'Intente nuevamente.');
            }else{
                $this->session->set_flashdata('message_success', 'Procedimiento realizado con exito.');
            }
            redirect('admin/users');
        }
    }
    
    public function update_password($id = NULL)
	{
        $this->form_validation->set_rules('password', 'Contrasena', 'required|min_length[6]|max_length[10]');
        $this->form_validation->set_rules('repeat_password', 'Confirmar Contraseña', 'required|matches[password]|min_length[6]|max_length[10]');

        $this->form_validation->set_error_delimiters('<small class="form-text " style="color:red">', '</small>');

        if ($this->form_validation->run() == FALSE) {
            $this->template->portal('admin/update_password');
        } else {
            $password   = $this->input->post('password');
            $user       = $this->encrypt->decode($id);

            $resp = $this->admin_model->update_password_entries($user, $password);

            if(!$resp){
                $this->session->set_flashdata('message_error', 'Intente nuevamente.');
            }else{
                $this->session->set_flashdata('message_success', 'Procedimiento realizado con exito.');
            }
            redirect('admin/users');
        }
    }

    //======================================================================
    // SESSION
    //======================================================================

    function valid_session() {

        if (!$this->session->userdata('logged')) {
            $this->close_sesion();
        }
    }
    
    public function close_sesion(){
        $this->session->sess_destroy();
        redirect('auth/access');
    }
	
}
