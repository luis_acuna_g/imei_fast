<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'form', 'file'));
        $this->load->library(array('form_validation', 'session','template', 'encrypt'));
        $this->load->model(array('auth_model'));
	}
	
	//======================================================================
    // LOGIN
    //======================================================================


	//-----------------------------------------------------
    // Sub-Category access LOGIN
	//-----------------------------------------------------
	
	public function access()
	{
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Contraseña', 'required|callback_user_login_check[' . $this->input->post('email') . ']');

        $this->form_validation->set_error_delimiters('<small class="form-text " style="color:red">', '</small>');

        if ($this->form_validation->run() == FALSE) {
            $this->template->auth('auth/access');
        } else {
            redirect('portal/work_orders');
        }
		
	}
	
	//======================================================================
    // CALLBACK
    //======================================================================


    //-----------------------------------------------------
    // Sub-Category user_login_check CALLBACK
    //-----------------------------------------------------

    public function user_login_check($key, $email) {
        $resp = $this->auth_model->login_user($key, $email );

        if (!$resp) {
            $this->form_validation->set_message('user_login_check', 'Los datos ingresados son inv&aacute;lidos, intente nuevamente.');
            return FALSE;
        } else {

            $newdata = array(
                'name' 	        => $resp->name,
                'identifier'    => $this->encrypt->encode($resp->id_user),
                'logged_in'     => TRUE,
                'administration' => ($resp->flag == 1) ? TRUE : FALSE
            );

            $this->session->set_userdata('logged', $newdata);

            return TRUE;
        }
    }
}
