<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'form', 'file', 'string'));
        $this->load->library(array('form_validation', 'session','template', 'encrypt'));
        $this->load->model(array('portal_model'));

        $this->valid_session();

    }
    
    public function work_orders(){
        $data['info_work_orders'] = $this->portal_model->get_work_orders_entries( $this->encrypt->decode($this->session->userdata['logged']['identifier']) );
        $this->template->portal('portal/work_orders', $data);
    }

    public function add_work_order()
	{
        $this->form_validation->set_rules('imei', 'IMEI', 'required|exact_length[15]|integer');
        $this->form_validation->set_rules('brand', 'Marca', 'required');
        $this->form_validation->set_rules('model', 'Modelo', 'required');

        $this->form_validation->set_error_delimiters('<small class="form-text " style="color:red">', '</small>');

        if ($this->form_validation->run() == FALSE) {
            $this->template->portal('portal/add_work_order');
        } else {
            $imei           = $this->input->post('imei'); 
            $imei_secondary       = $this->input->post('imei_secondary'); 
            $brand          = $this->input->post('brand');
            $model          = $this->input->post('model');
            $description    = $this->input->post('description');

            $resp = $this->portal_model->insert_work_order_entry($imei, $imei_secondary, $brand, $model, $description, $this->encrypt->decode($this->session->userdata['logged']['identifier']) );

            if(!$resp){
                $this->session->set_flashdata('message_error', 'Intente nuevamente.');
            }else{
                mail('botones152@gmail.com', 'IMEI FAST', 'Nueva orden de trabajo IMEI: '. $imei.' Marca:'.$brand.' Modelo: '.$model. ' Solicitante: '.$this->session->userdata['logged']['name']);
                $this->session->set_flashdata('message_success', 'Procedimiento realizado con exito.');
            }
            redirect('portal/work_orders');
        }
    }
	
	   
    //======================================================================
    // SESSION
    //======================================================================

    function valid_session() {

        if (!$this->session->userdata('logged')) {
            $this->close_sesion();
        }
    }
    
    public function close_sesion(){
        $this->session->sess_destroy();
        redirect('auth/access');
    }
	
}
