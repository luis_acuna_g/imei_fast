<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //======================================================================
    // LOGIN
    //======================================================================


    //-----------------------------------------------------
    // Sub-Category login_user LOGIN
    //-----------------------------------------------------
    
    function login_user($key, $email)
    {
        $this->db->where('email', $email);
        $this->db->where('password', md5($key) );
        $this->db->where('status', 1);
        $query = $this->db->get('m_users');

        if ($query->result()) {
            return $query->row();
        } else {
            return FALSE;
        }
    }
    
}



