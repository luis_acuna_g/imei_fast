<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //======================================================================
    // WORK ORDERS
    //======================================================================


    //-----------------------------------------------------
    // Sub-Category get_work_orders_entries WORK ORDERS
    //-----------------------------------------------------
    
    function get_work_orders_entries($user)
    {
        $this->db->where('id_user', $user);
        $query = $this->db->get('m_work_orders');
        if ($query->result()) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    //-----------------------------------------------------
    // Sub-Category insert_job_entry WORK ORDERS
    //-----------------------------------------------------

    function insert_work_order_entry($imei, $imei_scondary, $brand, $model, $description, $user)
    {
        $data = array(
            'id_user'   => $user,
            'imei'      => $imei,
            'imei_secondary' => $imei_scondary,
            'brand'     => strtoupper($brand),
            'model'     => strtoupper($model),
            'description'  => strtoupper($description),
            'status'    => 0,
            'date_create'   => date('Y-m-d H:i:s')
        );

        $this->db->insert('m_work_orders', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }


    //======================================================================
    // USERS
    //======================================================================


    //-----------------------------------------------------
    // Sub-Category get_users_entries USERS
    //-----------------------------------------------------
    
    function get_users_entries()
    {
        $query = $this->db->get('m_users');

        if ($query->result()) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    //-----------------------------------------------------
    // Sub-Category insert_user_entry USERS
    //-----------------------------------------------------
    
    function insert_user_entry($email, $phone, $name, $password)
    {
        $data = array(
            'name'      => strtoupper($name),
            'phone'     => $phone,
            'email'     => $email,
            'password'  => md5($password),
            'status'    => 1
        );

        $this->db->insert('m_users', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    //-----------------------------------------------------
    // Sub-Category update_password_entries USERS
    //-----------------------------------------------------

    function update_password_entries($user, $password){
        $data = array(
            'password' => md5($password)
        );

        $this->db->where('id_user', $user);
        $this->db->update('m_users', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
    
}