<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //======================================================================
    // WORK ORDERS
    //======================================================================


    //-----------------------------------------------------
    // Sub-Category get_work_orders_entries WORK ORDERS
    //-----------------------------------------------------
    
    function get_work_orders_entries()
    {
        $this->db->select('m_work_orders.imei, m_work_orders.imei_secondary, m_work_orders.brand, m_work_orders.model, m_work_orders.date_create, m_work_orders.status, m_work_orders.id_work_order, m_users.name as name_user, m_users.email');
        $this->db->from('m_work_orders');
        $this->db->join('m_users', 'm_users.id_user = m_work_orders.id_user');
        $query = $this->db->get();
        if ($query->result()) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    //-----------------------------------------------------
    // Sub-Category update_work_order_entries WORK ORDERS
    //-----------------------------------------------------

    function update_work_order_entries($order, $status){
        $data = array(
            'status' => $status
        );

        $this->db->where('id_work_order', $order);
        $this->db->update('m_work_orders', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    //-----------------------------------------------------
    // Sub-Category delete_work_order_entries WORK ORDERS
    //-----------------------------------------------------

    function delete_work_order_entries($order){
        $this->db->where('id_work_order', $order);
        $this->db->delete('m_work_orders');
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    //======================================================================
    // USERS
    //======================================================================


    //-----------------------------------------------------
    // Sub-Category get_users_entries USERS
    //-----------------------------------------------------
    
    function get_users_entries()
    {
        $query = $this->db->get('m_users');

        if ($query->result()) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    //-----------------------------------------------------
    // Sub-Category insert_user_entry USERS
    //-----------------------------------------------------
    
    function insert_user_entry($email, $phone, $name, $password)
    {
        $data = array(
            'name'      => strtoupper($name),
            'phone'     => $phone,
            'email'     => $email,
            'password'  => md5($password),
            'status'    => 1
        );

        $this->db->insert('m_users', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    //-----------------------------------------------------
    // Sub-Category update_password_entries USERS
    //-----------------------------------------------------

    function update_password_entries($user, $password){
        $data = array(
            'password' => md5($password)
        );

        $this->db->where('id_user', $user);
        $this->db->update('m_users', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
    
}