
  <div class="row no-gutter">
    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
    <div class="col-md-8 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-lg-7 mx-auto">
              <h1 class="login-heading mb-5 text-left">Bienvenido a IMEI FAST!</h1>
              <?php echo form_open('auth/access'); ?>
                <div class="form-label-group">
                  
                  <input type="email" name="email" class="form-control" >
                  <label for="inputEmail">Correo Electrónico</label>
                  <?php echo form_error('email'); ?>
                </div>

                <div class="form-label-group">
                  
                  <input type="password" name="password" class="form-control">
                  <label for="inputPassword">Contraseña</label>
                  <?php echo form_error('password'); ?>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Ingresar</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  
    