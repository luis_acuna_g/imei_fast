<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/pricing.css">

    <link href="<?php echo base_url(); ?>assets/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <div class="container">
      <a class="navbar-brand text-primary font-weight-bold" href="#">IMEI FAST</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()?>portal/work_orders">Ordenes Trabajo</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Sr(a). <?php echo  $this->session->userdata['logged']['name'] ?></a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

              <?php if($this->session->userdata['logged']['administration']){ ?>

                <h6 class="dropdown-header">Administraci&oacute;n</h6>
                <a class="dropdown-item" href="<?php echo base_url()?>admin/users"><i class="fas fa-users fa-xs"></i> Usuarios</a>
                <a class="dropdown-item" href="<?php echo base_url()?>admin/work_orders"><i class="fas fa-store-alt fa-xs"></i> Ordenes Trabajo</a>
              <?php } ?>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?php echo base_url()?>portal/close_sesion">Cerrar Sesion</a>
            </div>
          </li>

        </ul>
      </div>
    </div>
  </nav>
<div class="container">

    
