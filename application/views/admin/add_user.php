<div class="card shadow-lg">
  <div class="card-body">
    <h1 class="display-4 text-primary mb-4">Nuevo Usuario</h1>
    <?php echo form_open('admin/add_user'); ?>

        <div class="form-group">
            <label for="inputName">Nombre</label>
            <input type="name" class="form-control" name="name" >
            <?php echo form_error('name'); ?>
        </div>

        <div class="form-group">
            <label for="inputEmail">Email</label>
            <input type="email" class="form-control" name="email" aria-describedby="emailHelp">
            <?php echo form_error('email'); ?>
        </div>

        <div class="form-group">
            <label for="inputPhone">Teléfono</label>
            <input type="text" class="form-control" name="phone">
            <?php echo form_error('phone'); ?>
        </div>

        <div class="form-group">
            <label for="inputPassword">Contraseña</label>
            <input type="text" class="form-control" name="password" value="<?php echo random_string('alnum', 16); ?>" readonly>
            <?php echo form_error('password'); ?>

        </div>

        <button type="submit" class="btn btn-primary btn-lg btn-block">Agregar</button>

    </form>
  </div>
</div>