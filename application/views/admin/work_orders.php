<div class="card shadow-lg">
  <div class="card-body">
    <h1 class="display-4 text-primary mb-4">Ordenes Trabajo</h1>
    <table class="table table-striped table-bordered datatable" style="width:100%">
        <thead>
            <tr>
            <th scope="col">Usuario</th>
            <th scope="col">IMEI</th>
            <th scope="col">IMEI Secundario</th>
            <th scope="col">Marca</th>
            <th scope="col">Modelo</th>
            <th scope="col">Fecha Creación</th>
            <th scope="col">Estado</th>
            <th scope="col" width="7%"></th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($info_work_orders)){ ?>
            <?php foreach($info_work_orders as $value){ ?>
            <tr>
                <td><?php echo $value->name_user; ?></td>
                <td><?php echo $value->imei; ?></td>
                <td><?php echo $value->imei_secondary; ?></td>
                <td><?php echo $value->brand; ?></td>
                <td><?php echo $value->model; ?></td>
                <td><?php echo $value->date_create; ?></td>
                <td>
                    <?php
                      switch ($value->status) {
                        case 0:
                            echo '<span class="badge badge-warning">Pendiente</span>';
                            break;
                        case 1:
                            echo '<span class="badge badge-info">En proceso</span>';
                            break;
                        case 2:
                            echo '<span class="badge badge-danger">Error</span>';
                            break;
                        case 3:
                              echo '<span class="badge badge-success">OK</span>';
                              break;
                      }
                    
                    ?>
                </td>
                <td>
                    <a href="<?php echo base_url()?>admin/update_work_order/<?php echo $this->encrypt->encode($value->id_work_order);?>/<?php echo $this->encrypt->encode($value->email);?>"><i class="fas fa-edit"></i></a>
                    <a href="<?php echo base_url()?>admin/remove_work_order/<?php echo $this->encrypt->encode($value->id_work_order);?>" class="text-danger ml-2"><i class="far fa-trash-alt"></i></a>
                </td>
            </tr>
            <?php } ?>
            <?php } ?>
        </tbody>
    </table>
  </div>
</div>