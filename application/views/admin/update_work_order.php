<div class="card shadow-lg">
  <div class="card-body">
    <h1 class="display-4 text-primary mb-4">Modificar Orden </h1>

    <?php echo form_open('admin/update_work_order/'.$this->uri->segment(3).'/'.$this->uri->segment(4) ); ?>

        <div class="form-group">
            <label for="inputStatus">Estado</label>
            <select class="form-control" name="status">
                <option value="0">Pendiente</option>
                <option value="1">En proceso</option>
                <option value="2">Error</option>
                <option value="3">OK</option>
            </select>
            <?php echo form_error('status'); ?>

        </div>

        <button type="submit" class="btn btn-primary btn-lg btn-block">Modificar</button>

    </form>
  </div>
</div>