<div class="card shadow-lg">
  <div class="card-body">
    <h1 class="display-4 text-primary mb-4">Usuarios</h1>
    <table class="table table-striped table-bordered datatable" style="width:100%">
        <thead>
            <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Teféfono</th>
            <th scope="col" width="3%"></th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($info_users)){ ?>
            <?php foreach($info_users as $value){ ?>
            <tr>
                <th scope="row"><?php echo $value->name; ?></th>
                <td><?php echo $value->email; ?></td>
                <td><?php echo $value->phone; ?></td>
                <td>
                    <a href="<?php echo base_url() ?>admin/update_password/<?php echo $this->encrypt->encode($value->id_user); ?>"><i class="fas fa-key"></i></a>
                </td>
            </tr>
            <?php } ?>
            <?php } ?>
        </tbody>
    </table>
    <a class="btn btn-primary btn-lg btn-block mt-5" href="<?php echo base_url()?>admin/add_user">Nuevo Usuario</a>
  </div>
</div>