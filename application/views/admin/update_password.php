<div class="card shadow-lg">
  <div class="card-body">
    <h1 class="display-4 text-primary mb-4">Modificar Contraseña</h1>

    <?php echo form_open('admin/update_password/'.$this->uri->segment(3)); ?>

        <div class="form-group">
            <label for="inputPassword">Contraseña</label>
            <input type="password" class="form-control" name="password">
            <?php echo form_error('password'); ?>
        </div>

        <div class="form-group">
            <label for="inputPassword">Repite Contraseña</label>
            <input type="password" class="form-control" name="repeat_password">
            <?php echo form_error('repeat_password'); ?>

        </div>

        <button type="submit" class="btn btn-primary btn-lg btn-block">Modificar</button>

    </form>
  </div>
</div>