<div class="card shadow-lg">
  <div class="card-body">
    <h1 class="display-4 text-primary mb-4">Nueva Orden</h1>
    <?php echo form_open('portal/add_work_order'); ?>

        <div class="form-group">
            <label for="inputImei">IMEI (*)</label>
            <input type="name" class="form-control" name="imei" value="<?php echo set_value('imei'); ?>">
            <?php echo form_error('imei'); ?>
        </div>

        <div class="form-group">
            <label for="inputImei">IMEI Secundario</label>
            <input type="name" class="form-control" name="imei_secondary" value="<?php echo set_value('imei_secondary'); ?>">
            <?php echo form_error('imei_secondary'); ?>
        </div>

        <div class="form-group">
            <label for="inputBrand">Marca (*)</label>
            <input type="text" class="form-control" name="brand" value="<?php echo set_value('brand'); ?>">
            <?php echo form_error('brand'); ?>
        </div>

        <div class="form-group">
            <label for="inputModel">Modelo (*)</label>
            <input type="text" class="form-control" name="model" value="<?php echo set_value('model'); ?>">
            <?php echo form_error('model'); ?>
        </div>

        <div class="form-group">
            <label for="inputPassword">Descripción</label>
            <textarea class="form-control" name="description" rows="3"></textarea>
            <?php echo form_error('description'); ?>

        </div>

        <button type="submit" class="btn btn-primary btn-lg btn-block">Agregar</button>

    </form>
  </div>
</div>