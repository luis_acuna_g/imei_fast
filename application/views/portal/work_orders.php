<div class="card shadow-lg">
  <div class="card-body">
    <h1 class="display-4 text-primary mb-4">Ordenes Trabajo</h1>
    <table class="table table-striped table-bordered datatable" style="width:100%">
        <thead>
            <tr>
            <th scope="col">IMEI</th>
            <th scope="col">IMEI Secundario</th>
            <th scope="col">Marca</th>
            <th scope="col">Modelo</th>
            <th scope="col">Fecha Creación</th>
            <th scope="col">Estado</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($info_work_orders)){ ?>
            <?php foreach($info_work_orders as $value){ ?>
            <tr>
                <th scope="row"><?php echo $value->imei; ?></th>
                <th scope="row"><?php echo $value->imei_secondary; ?></th>
                <td><?php echo $value->brand; ?></td>
                <td><?php echo $value->model; ?></td>
                <td><?php echo $value->date_create; ?></td>
                <td>
                    <?php
                      switch ($value->status) {
                        case 0:
                            echo '<span class="badge badge-warning">Pendiente</span>';
                            break;
                        case 1:
                            echo '<span class="badge badge-info">En proceso</span>';
                            break;
                        case 2:
                            echo '<span class="badge badge-danger">Error</span>';
                            break;
                        case 3:
                              echo '<span class="badge badge-success">OK</span>';
                              break;
                      }
                    
                    ?>
                </td>
            </tr>
            <?php } ?>
            <?php } ?>
        </tbody>
    </table>
    <a class="btn btn-primary btn-lg btn-block mt-5" href="<?php echo base_url()?>portal/add_work_order">Nueva Orden Trabajo</a>
  </div>
</div>